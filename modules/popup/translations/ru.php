<?php

global $_MODULE;
$_MODULE = array();

$_MODULE['<{popup}prestashop>popup_536c84c82915b20017d4ea8d209f0a27'] = 'Всплывающее окно диапозона цен';
$_MODULE['<{popup}prestashop>popup_920233ac0677fcf6d73222b0bf60c788'] = 'Цена от (включительно)';
$_MODULE['<{popup}prestashop>popup_5cc3f58a38af84b74f3bc32a1d031b0f'] = 'Цена до (включительно)';
$_MODULE['<{popup}prestashop>popup_18ad6f609fb5fc02e8b40e9e69d5dcac'] = 'Значение \'От\' должно быть положительным числом.';
$_MODULE['<{popup}prestashop>popup_a00c8566ad73736871d5af8949162b51'] = 'Значение \'До\' должно быть положительным числом.';
$_MODULE['<{popup}prestashop>popup_39c41a1ceb356c8e9d95f40dcbf2e2b5'] = 'Значение \'От\' должно быть больше значения \'До\'.';

return $_MODULE;
