<?php
/**
 * Created by PhpStorm.
 * User: noname
 * Date: 10.04.2018
 * Time: 19:36
 */
if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

class popup extends Module
{
    public function __construct() {
        $this->name = 'popup';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'Exta5y';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Product popup');
        $this->description = $this->l('');
        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => '1.6.99.99'];
    }

    public function install() {
        return (
            parent::install()
            AND Configuration::updateValue('POPUP_FROM')
            && Configuration::updateValue('POPUP_TO')
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayTest')
        );
    }

    public function uninstall() {
        return (
            Configuration::deleteByName('POPUP_FROM')
            AND Configuration::deleteByName('POPUP_TO')
            AND parent::uninstall()
        );
    }

    public function getContent() {
        $output = '';
        $errors = [];
        if (Tools::isSubmit('submitModule')) {
            $from = Tools::getValue('popup_from');
            if (!Validate::isInt($from) || $from <= 0) {
                $errors[] = $this->l('\'From\' value must be positive number.');
            }

            $to = Tools::getValue('popup_to');
            if (!Validate::isInt($to) || $to <= 0) {
                $errors[] = $this->l('\'To\' value must be positive number.');
            }

            if (empty($errors)) {
                if ($from > $to) {
                    $errors[] = $this->l('\'From\' can\'t be greater than \'To\' value.');
                }
            }

            if (isset($errors) && count($errors)) {
                $output = $this->displayError(implode('<br />', $errors));
            } else {
                Configuration::updateValue('POPUP_FROM', Tools::getValue('popup_from', ''));
                Configuration::updateValue('POPUP_TO', Tools::getValue('popup_to', ''));
                Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('popup.tpl'));
                $output = $this->displayConfirmation($this->l('Your settings have been updated.'));
            }
        }

        return $output . $this->renderForm();
    }

    public function hookDisplayHeader($params) {
        $this->context->controller->addCSS($this->_path . 'test.css', 'all');
        $this->context->controller->addJS($this->_path . 'test.js');
    }

    public function hookDisplayTest() {
        $products = ProductCore::getProductsByPriceRange($this->context->language->id,
            Configuration::get('POPUP_FROM'),
            Configuration::get('POPUP_TO'),
            0, 0, 'id_product', 'ASC'
        );
        $products_c = count($products);

        if (!$this->isCached('popup.tpl', $this->getCacheId())) {
            $this->smarty->assign([
                'popup_from' => Configuration::get('POPUP_FROM'),
                'popup_to' => Configuration::get('POPUP_TO'),
                'products_count' => $products_c,
            ]);
        }

        return $this->display(__FILE__, 'popup.tpl', $this->getCacheId());
    }

    public function renderForm() {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Products (popup)'),
                    'icon' => 'icon-cogs',
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('Price from (inclusive)'),
                        'name' => 'popup_from',
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Price to (inclusive)'),
                        'name' => 'popup_to',
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                ],
            ],
        ];

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        return $helper->generateForm([$fields_form]);
    }

    public function getConfigFieldsValues() {
        return [
            'popup_from' => Tools::getValue('popup_from', Configuration::get('POPUP_FROM')),
            'popup_to' => Tools::getValue('popup_to', Configuration::get('POPUP_TO')),
        ];
    }
}